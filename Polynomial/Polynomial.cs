﻿using System;
using System.Linq;
using System.Text;

namespace Polynomial
{
    class Polynomial
    {
        private readonly PolynomialElement[] _polynomial;


        #region Constructors

        public Polynomial(int power, double[] coefficients)
        {
            if (coefficients.Count() != (power + 1))
            {
                throw new ArgumentException("Count of coefficients must be equal to (power + 1)");
            }

            _polynomial = new PolynomialElement[power + 1];

            for (int i = 0; i <= power; i++)
            {
                _polynomial[power - i] = new PolynomialElement(coefficients[power - i], i);
            }
        }

        protected Polynomial(PolynomialElement[] polynomial)
        {
            _polynomial = new PolynomialElement[polynomial.Length];
            polynomial.CopyTo(_polynomial,0);
        }

        #endregion

        #region Operators

        public static Polynomial operator *(Polynomial left, Polynomial right)
        {
            return new Polynomial(left._polynomial
                .SelectMany(r => right._polynomial
                    .Select(e => new PolynomialElement(r.Coef * e.Coef, e.Pow + r.Pow)))
                .GroupBy(e => e.Pow)
                .Select(i => new PolynomialElement(i.Sum(e => e.Coef), i.FirstOrDefault().Pow))
                .ToArray());
        }

        public static Polynomial operator +(Polynomial left, Polynomial right)
        {
            return new Polynomial(left._polynomial
                .Concat(right._polynomial)
                .GroupBy(e => e.Pow)
                .Select(i => new PolynomialElement(i.Sum(e => e.Coef), i.FirstOrDefault().Pow))
                .ToArray());
        }

        public static Polynomial operator -(Polynomial left, Polynomial right)
        {
            return new Polynomial(left._polynomial
                .Concat(right._polynomial.Select(e => new PolynomialElement(-e.Coef, e.Pow)))
                .GroupBy(e => e.Pow)
                .Select(i => new PolynomialElement(i.Sum(e => e.Coef), i.FirstOrDefault().Pow))
                .ToArray());
        }

        #endregion

        #region Methods

        public double Calculate(double argument)
        {
            return _polynomial.Sum(i => i.Coef * Math.Pow(argument, i.Pow));
        }


        public override string ToString()
        {
            StringBuilder polyString = new StringBuilder();

            foreach (PolynomialElement polyElement in _polynomial.OrderByDescending(e => e.Pow))
            {
                if (polyElement.Pow > 1)
                {
                    polyString.Append(polyElement.Coef.ToString("+ 0.#;- 0.#;") + " * x^" + polyElement.Pow + " ");
                }
                else if (polyElement.Pow == 1)
                {
                    polyString.Append(polyElement.Coef.ToString("+ 0.#;- 0.#;") + " * x ");
                }
                else if (polyElement.Pow == 0)
                {
                    polyString.Append(polyElement.Coef.ToString("+ 0.#;- 0.#;" + " "));
                }
            }

            return polyString.ToString();
        }

        #endregion


        protected struct PolynomialElement
        {
            public double Coef;
            public int Pow;

            public PolynomialElement(double coefficient, int power)
            {
                Coef = coefficient;
                Pow = power;
            }
        }
    }
}
