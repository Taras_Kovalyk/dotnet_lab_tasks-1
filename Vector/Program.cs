﻿using System;


namespace Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Vector( -2, 2);
            var b = new Vector(-2, 2);

            a[-2] = 10;
            a[-1] = 20;
            a[0] = 30;
            a[1] = 40;
            a[2] = 50;


            b[-2] = 100;
            b[-1] = 200;
            b[0] = 300;
            b[1] = 400;
            b[2] = 500;

            Console.Write("a:");
            Console.WriteLine(a);

            Console.Write("\nb:");
            Console.WriteLine(b);

            Console.WriteLine("\n\n" + new string('-', 50));

            Vector result1 = a + b;
            Vector result2= a - b;
            Vector result3 = a * 2;

            Console.WriteLine("\na + b:" + result1);
            Console.WriteLine("\na - b:" + result2);
            Console.WriteLine("\na * 2:" + result3);
            Console.WriteLine("\nVectors a and b are equal element by element: " + a.IsEqualTo(b));

            Console.WriteLine("\n" + new string('-',50));




            Console.ReadKey();
        }
    }
}
