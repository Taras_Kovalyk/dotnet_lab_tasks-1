﻿using System;
using System.Text;


namespace Vector
{
    public class Vector
    {
        private readonly int[] _vector;
        private readonly bool _userInterval;
        private int _leftBound;
        private int _rightBound;

        #region Constructors

        public Vector() { }


        public Vector(int size)
        {
            _vector = new int[size];
        }


        public Vector(int[] vector) : this(vector.Length)
        {
            vector.CopyTo(_vector, 0);
        }


        public Vector(int leftBound, int rightBound)
        {
            if(rightBound > leftBound)
            {
                _leftBound = leftBound;
                _rightBound = rightBound;
                _userInterval = true;

                _vector = new int[rightBound - leftBound + 1];
            }
            else
            {
                throw new ArgumentException("Value of right boundary must be bigger than value of left");
            }
        }



        #endregion

        #region Properties

        /// <summary>
        /// Get lenght of vector
        /// </summary>
        public int Length => _vector.Length;

        /// <summary>
        /// Check whether current vector has user defined interval
        /// </summary>
        public bool UserInterval => _userInterval;

        /// <summary>
        /// Get left boundary value of vector
        /// </summary>
        public int LeftBound
        {
            get
            {
                if (_userInterval)
                {
                    return _leftBound;
                }

                return 0;

            }
            set
            {
                if (_userInterval)
                {
                    _leftBound = value;
                }

                else
                {
                    throw new Exception(
                        "You can't set left boundary to vector that doesn't have user defined interval. Left boundary is 0 by default.");
                }
            }
        }

        /// <summary>
        /// Get right boundary value of vector
        /// </summary>
        public int RightBound
        {
            get
            {
                if (_userInterval)
                {
                    return _rightBound;
                }

                return _vector.Length;

            }
            set
            {
                if (_userInterval)
                {
                    _rightBound = value;
                }

                else
                {
                    throw new Exception(
                        "You can't set right boundary to vector that doesn't have user defined interval. Right boundary is equal to vector's length by default.");
                }
            }
        }

        /// <summary>
        /// Get or set the element with the specified index
        /// </summary>
        public int this[int i]
        {
            get
            {
                if (_userInterval)
                {
                    if (i >= _leftBound && i <= _rightBound)
                    {
                        return _vector[i + Math.Abs(_leftBound)];
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }


                if (i >= 0 && i < _vector.Length)
                {
                    return _vector[i];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (_userInterval)
                {
                    if (i >= _leftBound && i <= _rightBound)
                    {
                        _vector[i + Math.Abs(_leftBound)] = value;
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }


                else{
                    if (i >= 0 && i < _vector.Length)
                    {
                        _vector[i] = value;
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }
            }
            
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add two vectors with user defined similar intervals
        /// </summary>
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            if ((vector1.LeftBound == vector2.LeftBound) && (vector1.RightBound == vector2.RightBound))
            {
                if (vector1.UserInterval && vector2.UserInterval)
                {
                    var resultVector = new Vector(vector1.LeftBound, vector1.RightBound);

                    for (var i = resultVector.LeftBound; i <= resultVector.RightBound; i++)
                    {
                        resultVector[i] = vector1[i] + vector2[i];
                    }

                    return resultVector;
                }
                else
                {
                    throw new ArgumentException("To add two vectors, both of them must have user defined intervals");
                }
            }

            throw new ArgumentException("Vectors must have similar intervals of indexes");

        }


        /// <summary>
        /// Substract two vectors with user defined similar intervals
        /// </summary>
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            if ((vector1.LeftBound == vector2.LeftBound) && (vector1.RightBound == vector2.RightBound))
            {
                if (vector1.UserInterval && vector2.UserInterval)
                {
                    var resultVector = new Vector(vector1.LeftBound, vector1.RightBound);

                    for (var i = resultVector.LeftBound; i <= resultVector.RightBound; i++)
                    {
                        resultVector[i] = vector1[i] - vector2[i];
                    }

                    return resultVector;
                }
                else
                {
                    throw new ArgumentException("To add two vectors, both of them must have user defined intervals");
                }
            }

            throw new ArgumentException("Vectors must have similar intervals of indexes");

        }

        /// <summary>
        /// Myltiply all elements of vector by number
        /// </summary>
        public static Vector operator *(Vector vector, int number)
        {
            Vector resultVector;

            if (vector.UserInterval)
            {
                resultVector = new Vector(vector.LeftBound, vector.RightBound);

                for (var i = vector.LeftBound; i <= vector.RightBound; i++)
                {
                    resultVector[i] = vector[i] * number;
                }
            }

            else
            {
                resultVector = new Vector(vector.Length);

                for (var i = 0; i < vector.Length; i++)
                {
                    resultVector[i] = vector[i] * number;
                }
            }
            

            return resultVector;
        }

        /// <summary>
        /// Check whether elements of given vector are equal to elements of current vector.
        /// </summary>
        public bool IsEqualTo(Vector comparedVector)
        {
            if (_vector.Length == comparedVector.Length)
            {
                for (var i = 0; i < _vector.Length; i++)
                {
                    if (_vector[i] != comparedVector[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override string ToString()
        {
            StringBuilder outputVector = new StringBuilder();

            for (var i = 0; i < _vector.Length; i++)
            {
                outputVector.Append("\t" + _vector[i]);
            }

            return outputVector.ToString();
        }

        #endregion



    }
}
