﻿using System;
using System.Globalization;

namespace Rectangle
{
    public class Rectangle
    {
        private float _x;
        private float _y;
        private float _width;
        private float _height;


        public Rectangle() { }


        public Rectangle(float x, float y, float width, float height)
        {
            _x = x;
            _y = y;
            _width = width;
            _height = height;
        }


        #region Properties

        /// <summary>
        ///    Gets or sets the x-coordinate of the upper-left corner of the rectangle
        /// </summary>
        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        /// <summary>
        ///    Gets or sets the y-coordinate of the upper-left corner of the rectangle
        /// </summary>
        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public float Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public float Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        ///       Gets the x-coordinate of the upper-left corner of the rectangle
        /// </summary>
        public float Left => X;

        
        /// <summary>
        ///       Gets the y-coordinate of the upper-left corner of the rectangle
        /// </summary>
        public float Top => Y;

        
        /// <summary>
        ///       Gets the x-coordinate of the lower-right corner of the rectangle
        /// </summary>
        public float Right => X + Width;

        
        /// <summary>
        ///       Gets the y-coordinate of the lower-right corner of the rectangle
        /// </summary>
        public float Bottom => Y + Height;

        #endregion

        #region Methods

        public void Move(float x, float y)
        {
            X += x;
            Y += y;
        }

        public void Resize(float width, float height)
        {
            Width += width;
            Height += height;
        }

        /// <summary>
        /// Get intersection between two rectangles
        /// </summary>
        public static Rectangle Intersect(Rectangle a, Rectangle b)
        {
            float x1 = Math.Max(a.X, b.X);
            float y1 = Math.Max(a.Y, b.Y);
            float x2 = Math.Min(a.X + a.Width, b.X + b.Width);
            float y2 = Math.Min(a.Y + a.Height, b.Y + b.Height);

            if (x2 >= x1 && y2 >= y1)
            {
                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            return new Rectangle();
        }
        /// <summary>
        /// Get smallest rectangle which contains two given
        /// </summary>
        public static Rectangle Union(Rectangle a, Rectangle b)
        {

            float x1 = Math.Min(a.X, b.X);
            float x2 = Math.Max(a.X + a.Width, b.X + b.Width);
            float y1 = Math.Min(a.Y, b.Y);
            float y2 = Math.Max(a.Y + a.Height, b.Y + b.Height);

            return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }


        public override string ToString()
        {
            return "{X=" + X.ToString(CultureInfo.CurrentCulture) + ", Y=" + Y.ToString(CultureInfo.CurrentCulture) +
                    ", Width=" + Width.ToString(CultureInfo.CurrentCulture) +
                    ", Height=" + Height.ToString(CultureInfo.CurrentCulture) + "}";
        }

        #endregion
    }
}
