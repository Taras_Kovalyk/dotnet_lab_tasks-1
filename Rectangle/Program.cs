﻿using System;



namespace Rectangle
{
    class Program
    {
        static void Main()
        {
            var rect1 = new Rectangle(4, 4, 4, 2);
            var rect2 = new Rectangle(6, 4, 4, 3);

            Console.WriteLine("Rectangle 1:");
            Console.WriteLine(rect1);
            Console.WriteLine();

            Console.WriteLine("Rectangle 2:");
            Console.WriteLine(rect2);
            Console.WriteLine("\n"+new string('-', 50) + "\n");

            Console.WriteLine("Intersection between rectangle 1 and 2:");
            Rectangle intersect = Rectangle.Intersect(rect1, rect2);
            Console.WriteLine(intersect);
            Console.WriteLine();

            Console.WriteLine("Union between rectangle 1 and 2:");
            Rectangle union = Rectangle.Union(rect1, rect2);
            Console.WriteLine(union);
            Console.WriteLine();

            Console.WriteLine("Resize rectangle 1:");
            rect1.Resize(5, 6);
            Console.WriteLine(rect1);
            Console.WriteLine();

            Console.WriteLine("Move rectangle 2 to (0;0) coordinates:");
            rect2.Move(-6, -4);
            Console.WriteLine(rect2);
            Console.WriteLine("\n" + new string('-', 50) + "\n");

            Console.Read();
        }
    }
}
